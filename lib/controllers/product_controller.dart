import 'package:tiki/models/product.dart';
import 'package:tiki/services/product_service.dart';

class ProductController {
  // we don't instantiate this controller class, but calling it through
  // the Product Provider

  final ProductService _service = ProductService();

  // This public getter cannot be modified by any other object
  // (kinda like private, but still accessible static, you can see it but no operation allowed)
  List<Product> get products => List.unmodifiable(_service.getAllProducts());

  void createProduct(String name, String image, String category, int price,
      int quantity, bool isOnSale) {
    _service.createProduct(name, image, category, price, quantity, isOnSale);
  }

  void saveProduct(Product product) {
    _service.saveProduct(product);
  }

  void deleteProduct(Product product) {
    _service.deleteProduct(product);
  }

// String checkDuplicated(Iterable<String> names, String newName) {
//   final duplicatedCount =
//       names.where((name) => name.contains(newName, 0)).length;
//   if (duplicatedCount > 0) {
//     // Problem: not very good since if we suggest newName2 maybe, but the client rename it to newName3,
//     // then another newName comes and we will suggest another newName3 which makes it duplicated again maybe causing a weird loop...?
//     newName += ' (${duplicatedCount + 1})';
//     return newName;
//     // Solution: scan the number too and fill in the missing number sequentially
//     // since we are already scanning through the list using where, I guess it won't affect the performance that much
//     // not doing it yet :)
//   }
//   return newName;
// }
}
