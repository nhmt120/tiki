import 'package:flutter/material.dart';
import 'package:tiki/ui/home_screen/home_screen.dart';

class Tiki extends StatefulWidget {
  const Tiki({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return TikiState();
  }
}

class TikiState extends State<Tiki> {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Tiki',
      home: Scaffold(
        body: HomeScreen(),
      ),
    );
  }
}
