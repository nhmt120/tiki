import 'package:flutter/material.dart';

class TransparentAppBar extends StatefulWidget {
  const TransparentAppBar({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _TransparentAppBarState();
  }
}

class _TransparentAppBarState extends State<TransparentAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.transparent,
      shadowColor: Colors.transparent,
      elevation: 0,
      iconTheme: const IconThemeData(
        color: Colors.black,
      ),
      actions: <Widget>[
        IconButton(
          icon: const Icon(Icons.notifications),
          tooltip: 'Notifications',
          onPressed: () {},
        ),
        IconButton(
          icon: const Icon(Icons.shopping_cart),
          tooltip: 'Shopping Cart',
          onPressed: () {},
        ),
      ],
    );
  }
}
