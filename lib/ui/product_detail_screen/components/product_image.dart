import 'package:flutter/material.dart';
import 'package:tiki/models/product.dart';

class ProductImage extends StatefulWidget {
  final Product product;

  const ProductImage({Key? key, required this.product}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ProductImageState();
  }
}

class _ProductImageState extends State<ProductImage> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 250,
      child: AspectRatio(
        aspectRatio: 1,
        child: Hero(
          tag: '',
          child: Image.network(widget.product.image),
        ),
      ),
    );
  }
}
