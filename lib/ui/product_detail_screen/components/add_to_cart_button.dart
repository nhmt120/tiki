import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tiki/models/product.dart';

class AddToCartButton extends StatefulWidget {
  final Product product;

  const AddToCartButton({Key? key, required this.product}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return AddToCartButtonState();
  }
}

class AddToCartButtonState extends State<AddToCartButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blue,
      height: 70.0,
      width: double.infinity,
      padding: const EdgeInsets.only(top: 5, bottom: 5),

      child: FittedBox(
        child: FloatingActionButton.extended(
          extendedPadding: EdgeInsets.only(left: 117, right: 117),
          onPressed: () {},
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0))
          ),
          backgroundColor: Colors.red,
          label: const Text('Chọn mua'),
        ),
      ),
    );
  }
}
