import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tiki/models/product.dart';
import 'package:tiki/ui/product_detail_screen/components/add_to_cart_button.dart';
import 'package:tiki/ui/product_detail_screen/components/product_image.dart';
import 'package:tiki/ui/product_detail_screen/components/product_info.dart';
import 'package:tiki/ui/product_detail_screen/components/transparent_app_bar.dart';

class DetailScreen extends StatefulWidget {
  const DetailScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _DetailScreenState();
  }
}

class _DetailScreenState extends State<DetailScreen> {

  @override
  void initState() {
    // hide notifications bar
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.leanBack);

    // to re-show bars
    // SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
  }

  @override
  Widget build(BuildContext context) {
    Product product = ModalRoute.of(context)!.settings.arguments as Product;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(100),
        child: TransparentAppBar(),
      ),
      extendBodyBehindAppBar: true,
      // extendBody: true,
      body: Column(
        children: [
          ProductImage(product: product),
          ProductInfo(product: product),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: AddToCartButton(product: product),
    );
  }


}
