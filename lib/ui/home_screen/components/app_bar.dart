import 'package:flutter/material.dart';

class HomeAppBar extends StatefulWidget implements PreferredSizeWidget {
  // implements: is to fix the error when Scaffold.appBar calls this class
  const HomeAppBar({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _HomeAppBarState();
  }

  @override
  Size get preferredSize => const Size.fromHeight(100);
}

class _HomeAppBarState extends State<HomeAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Image.asset(
        'assets/images/tiki-white-logo.png',
        width: 100,
        height: 35,
      ),
      leading: Container(
        child: Image.asset('assets/images/free-ship.png'),
        margin: const EdgeInsets.all(5),
      ),
      leadingWidth: 125,
      centerTitle: true,
      actions: <Widget>[
        IconButton(
          icon: const Icon(Icons.notifications),
          tooltip: 'Notifications',
          onPressed: () {},
        ),
        IconButton(
          icon: const Icon(Icons.shopping_cart),
          tooltip: 'Shopping Cart',
          onPressed: () {},
        ),
      ],
    );
  }
}
