import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class HomeBanner extends StatefulWidget {
  const HomeBanner({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _HomeBannerState();
  }
}

class _HomeBannerState extends State<HomeBanner> {
  List<Color> bannerColors = [
    Colors.deepPurple,
    Colors.red,
    Colors.green,
    Colors.yellow,
    Colors.orange
  ];

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(
        height: 100.0,
        enlargeCenterPage: true,
        onPageChanged: (position, reason) {
          // print(reason);
          // print(CarouselPageChangedReason.controller);
        },
        enableInfiniteScroll: true,
        autoPlay: true,
      ),
      items: bannerColors.map<Widget>((i) {
        return Builder(
          builder: (BuildContext context) {
            return Container(
              // height: 100.0,
              decoration: BoxDecoration(
                color: i,
                borderRadius: BorderRadius.circular(10),
              ),
            );
          },
        );
      }).toList(),
    );
  }
}
