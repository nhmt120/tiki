import 'package:flutter/material.dart';
import 'package:tiki/controllers/product_controller.dart';
import 'package:tiki/models/product.dart';
import 'package:tiki/product_provider.dart';
import 'package:tiki/ui/product_detail_screen/product_detail_screen.dart';

class ProductGrid extends StatefulWidget {
  const ProductGrid({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ProductGridState();
  }
}

class ProductGridState extends State<ProductGrid> {
  @override
  Widget build(BuildContext context) {
    generateProducts();
    return _buildGrid();
  }

  _buildGrid() {
    final products = ProductProvider.of(context).products;

    return SliverGrid(
      gridDelegate:
          const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          Product product = products[index];
          return InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DetailScreen(),
                  settings: RouteSettings(arguments: product),
                ),
              );
            },
            child: Card(
              // color: Colors.amber,
              child: Column(
                children: [
                  _gridProductImage(product),
                  _gridProductName(product),
                  _gridProductPrice(product),
                ],
              ),
            ),
          );
        },
        childCount: products.length,
        semanticIndexOffset: 2,
      ),
    );
  }

  _gridProductPrice(Product product) {
    return Container(
      alignment: Alignment.centerLeft,
      child: Text(
        '${product.price} \$',
        style: const TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: Colors.red,
        ),
      ),
      padding: const EdgeInsets.only(left: 5.0, top: 5.0),
    );
  }

  _gridProductName(Product product) {
    return Container(
      alignment: Alignment.centerLeft,
      child: Text(
        product.name,
        style: const TextStyle(fontSize: 20),
      ),
      padding: const EdgeInsets.only(left: 5.0),
    );
  }

  _gridProductImage(Product product) {
    return Container(
      // margin: const EdgeInsets.all(2.0),
      height: 120.0,
      decoration: _productImage(product),
      alignment: Alignment.center,
    );
  }

  _productImage(Product product) {
    return BoxDecoration(
      image: DecorationImage(
        // image: AssetImage('assets/images/items/glasses/kisspng-ray-ban-aviator-sunglasses-mirrored-sunglasses-png-file-sunglasses-5ab02b16d907d7.584802871521494806889.png'),
        image: NetworkImage(product.image),
        fit: BoxFit.fitWidth,
      ),
    );
  }

// getImages() async {
//   Directory dir = Directory('assets/images/items/glasses');
//   List<String> nameList = [];
//
//   await for (var entity in dir.list(recursive: true, followLinks: false)) {
//     String fileName = File(entity.path).uri.pathSegments.last;
//     print(entity.path);
//     imageList.add('assets/images/items/glasses/' + fileName);
//   }
//   return nameList;
// }

  generateProducts() {
    ProductController controller = ProductProvider.of(context);
    // for testing, gonna fetch data from api later
    controller.createProduct(
      'Fancy Ice Cube',
      'https://www.teahub.io/photos/full/92-924673_beautiful-doodle-art-by-lei-melendres-doodle-art.png',
      'F&B',
      199,
      5,
      false,
    );
    controller.createProduct(
      'RJ45 Connector',
      'https://salt.tikicdn.com/cache/400x400/ts/product/c6/64/7e/786bd21735fcfc609724174e598e3b08.jpg.webp',
      'Electronics',
      10,
      20,
      false,
    );
    controller.createProduct(
      'Macbook',
      'https://cdn.pixabay.com/photo/2014/09/24/14/29/macbook-459196__340.jpg',
      'Electronics',
      2999,
      10,
      true,
    );
  }
}
