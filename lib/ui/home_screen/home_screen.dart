import 'package:flutter/material.dart';
import 'package:tiki/ui/home_screen/components/bottom_navigator.dart';

import 'components/app_bar.dart';
import 'components/banner.dart';
import 'components/search_bar.dart';
import 'components/product_grid.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen> {
  // int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: HomeAppBar(),
      backgroundColor: Colors.blue,
      bottomNavigationBar: BottomNavigator(),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverPadding(padding: const EdgeInsets.all(10), sliver: SearchBar()),
          const SliverPadding(padding: EdgeInsets.all(2)),
          SliverToBoxAdapter(child: HomeBanner()),
          const SliverPadding(padding: EdgeInsets.all(6)),
          ProductGrid(),
        ],
      ),
    );
  }
}
