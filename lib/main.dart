import 'package:flutter/material.dart';
import 'package:tiki/product_provider.dart';
import 'package:tiki/ui/app.dart';

void main() {
  ProductProvider productProvider = ProductProvider(child: Tiki());
  runApp(productProvider);
}